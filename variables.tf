variable "cidr" {
  type = string
}

variable "name_prefix" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

# variable "private_subnets" {
#   type = list(string)
# }

variable "default_security_group_ingress" {
  type = map(object({
    protocol    = string
    from_port   = string
    to_port     = string
    cidr_blocks = list(string)
  }))
}

variable "default_security_group_egress" {
  type = map(object({
    protocol    = string
    from_port   = string
    to_port     = string
    cidr_blocks = list(string)
  }))
}


// ==================


variable "internal_lb" {
  type        = bool
  description = "Whether this load balancer is internal or internet-facing"
}

# variable "subnets_id" {
#   type        = list(string)
#   description = "A list of subnet IDs to attach to the LB"
# }

variable "listener_port" {
  type        = number
  description = "Port on which load balancer listens on"
}

variable "listener_protocol" {
  type        = string
  description = "Protocol for connections from clients to the load balancer"
}

# variable "target_vpc_id" {
#   type        = string
#   description = "ID of the VPC that this target group takes effect"
# }

variable "target_port" {
  type        = number
  description = "Port on which load balancer listens on"
}

variable "target_protocol" {
  type        = string
  description = "Protocol for connections from clients to the load balancer"
}


variable "task_definitions" {
  type = map(object({
    launch_types   = set(string)
    network_mode   = string
    memory         = number
    cpu            = number
    container_name = string
    container_port = number
    host_port      = number
  }))
}

variable "backend_container_image_url" {
  type = string
}

variable "frontend_container_image_url" {
  type = string
}

#-------------------------------#
# ECS service
#-------------------------------#
variable "ecs_services" {
  type = map(object({
    launch_type        = string
    task_definition    = string
    desired_task_count = number
    attach_lb          = bool
    container_port     = number
  }))
}