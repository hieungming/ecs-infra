cidr = "10.0.0.0/24"

name_prefix = "hieunm"

public_subnets  = ["10.0.0.0/28", "10.0.0.32/28"]
# private_subnets = ["10.0.0.16/28", "10.0.0.48/28"]

default_security_group_ingress = {
  "ingressRule1" = {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
default_security_group_egress = {
  "egressRule1" = {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# subnets_id = ["10.0.0.0/28", "10.0.0.32/28"]
internal_lb       = false
listener_port     = 8080
listener_protocol = "HTTP"
target_port       = 8080
target_protocol   = "HTTP"
# target_vpc_id = "vpc-0b95246c0411de316"


task_definitions = {
  "go-backend" = {
    launch_types   = ["FARGATE"]
    network_mode   = "awsvpc"
    memory         = 2048
    cpu            = 1024
    container_name = "go-backend"
    container_port = 8080
    host_port      = 8080
  }
  "go-frontend" = {
    launch_types   = ["FARGATE"]
    network_mode   = "awsvpc"
    memory         = 4096
    cpu            = 2048
    container_name = "go-frontend"
    container_port = 3000
    host_port      = 3000
  }
}

ecs_services = {
  "go-backend" = {
    launch_type        = "FARGATE"
    task_definition    = "go-backend"
    desired_task_count = 1
    attach_lb          = true
    container_port     = 8080
  }
  "go-frontend" = {
    launch_type        = "FARGATE"
    task_definition    = "go-frontend"
    desired_task_count = 1
    attach_lb          = false
    container_port     = 3000
  }
}

backend_container_image_url = "023221543387.dkr.ecr.us-east-1.amazonaws.com/go:be"
frontend_container_image_url = "023221543387.dkr.ecr.us-east-1.amazonaws.com/go:fe"